#ifndef HEADER_H
#define HEADER_H 

#include<iostream>
#include<windows.h>
#include<conio.h>
#include<fstream>
#include<cstring>
#include<cstdio>
#include<cstdlib>
#include<iomanip>

#define getch _getch

using namespace std;

//global variable declaration
extern COORD coord;
extern int k;
extern int r;
extern int flag;

extern ofstream fout;
extern ifstream fin;

inline void gotoxy(int x, int y)
{
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}

struct date
{
    int mm, dd, yy;
};


class item
{
private:
    int itemno;
    char name[25];
    date d;
public:

    int getItemno();
    char getNme();
    void setItemno(int i);
    void setName(char n);

    void add();
    void show();
    void report();
    int retno();
};

class amount : public item
{
private:
    float price, qty, tax, gross, dis, netamt;
public:
    void setPrice(float p);
    void setQty(float q);
    void setTax(float t);
    void setGross(float g);
    void setDis(float d);
    void setNetamt(float ne);

    float getPrice();
    float getQty();
    float getTax();
    float getGross();
    float getDis();
    float getNetamt();
    void add();
    void show();
    void report();
    void calculate();
    void pay();
    float retnetamt();
};

#endif